#
# Defines environment variables.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Ensure that a non-login, non-interactive shell has a defined environment.
if [[ ( "$SHLVL" -eq 1 && ! -o LOGIN ) && -s "${ZDOTDIR:-$HOME}/.zprofile" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprofile"
fi

PATH=$PATH:$HOME/.local/bin

# RVM
PATH=$PATH:$HOME/.rvm/bin

# Rust
PATH=$PATH:$HOME/.cargo/bin

# Go
PATH=$PATH:$HOME/go/bin

# Nix
export NIX_REMOTE=daemon
